// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RubeGoldberg/Recipe.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRecipe() {}
// Cross Module References
	RUBEGOLDBERG_API UScriptStruct* Z_Construct_UScriptStruct_FRecipe();
	UPackage* Z_Construct_UPackage__Script_RubeGoldberg();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
	RUBEGOLDBERG_API UScriptStruct* Z_Construct_UScriptStruct_FRecipeItem();
// End Cross Module References

static_assert(std::is_polymorphic<FRecipe>() == std::is_polymorphic<FTableRowBase>(), "USTRUCT FRecipe cannot be polymorphic unless super FTableRowBase is polymorphic");

	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_Recipe;
class UScriptStruct* FRecipe::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_Recipe.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_Recipe.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FRecipe, Z_Construct_UPackage__Script_RubeGoldberg(), TEXT("Recipe"));
	}
	return Z_Registration_Info_UScriptStruct_Recipe.OuterSingleton;
}
template<> RUBEGOLDBERG_API UScriptStruct* StaticStruct<FRecipe>()
{
	return FRecipe::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FRecipe_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UECodeGen_Private::FStructPropertyParams NewProp_Ingredients_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Ingredients_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Ingredients;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ResultIngredients_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ResultIngredients_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ResultIngredients;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRecipe_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Recipe.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRecipe_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRecipe>();
	}
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_Ingredients_Inner = { "Ingredients", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRecipeItem, METADATA_PARAMS(nullptr, 0) }; // 3891100002
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_Ingredients_MetaData[] = {
		{ "Category", "Recipe" },
		{ "ModuleRelativePath", "Recipe.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_Ingredients = { "Ingredients", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRecipe, Ingredients), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_Ingredients_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_Ingredients_MetaData)) }; // 3891100002
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_ResultIngredients_Inner = { "ResultIngredients", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRecipeItem, METADATA_PARAMS(nullptr, 0) }; // 3891100002
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_ResultIngredients_MetaData[] = {
		{ "Category", "Recipe" },
		{ "ModuleRelativePath", "Recipe.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_ResultIngredients = { "ResultIngredients", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRecipe, ResultIngredients), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_ResultIngredients_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_ResultIngredients_MetaData)) }; // 3891100002
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRecipe_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_Ingredients_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_Ingredients,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_ResultIngredients_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRecipe_Statics::NewProp_ResultIngredients,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRecipe_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RubeGoldberg,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"Recipe",
		sizeof(FRecipe),
		alignof(FRecipe),
		Z_Construct_UScriptStruct_FRecipe_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRecipe_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRecipe_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRecipe_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRecipe()
	{
		if (!Z_Registration_Info_UScriptStruct_Recipe.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_Recipe.InnerSingleton, Z_Construct_UScriptStruct_FRecipe_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_Recipe.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_Recipe_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_Recipe_h_Statics::ScriptStructInfo[] = {
		{ FRecipe::StaticStruct, Z_Construct_UScriptStruct_FRecipe_Statics::NewStructOps, TEXT("Recipe"), &Z_Registration_Info_UScriptStruct_Recipe, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FRecipe), 735384298U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_Recipe_h_2397777688(TEXT("/Script/RubeGoldberg"),
		nullptr, 0,
		Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_Recipe_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_Recipe_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
