// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RUBEGOLDBERG_Recipe_generated_h
#error "Recipe.generated.h already included, missing '#pragma once' in Recipe.h"
#endif
#define RUBEGOLDBERG_Recipe_generated_h

#define FID_RubeGoldberg_Source_RubeGoldberg_Recipe_h_11_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRecipe_Statics; \
	RUBEGOLDBERG_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> RUBEGOLDBERG_API UScriptStruct* StaticStruct<struct FRecipe>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_RubeGoldberg_Source_RubeGoldberg_Recipe_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
