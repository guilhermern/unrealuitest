// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RubeGoldberg/RubeGoldbergCharacter.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRubeGoldbergCharacter() {}
// Cross Module References
	RUBEGOLDBERG_API UClass* Z_Construct_UClass_ARubeGoldbergCharacter_NoRegister();
	RUBEGOLDBERG_API UClass* Z_Construct_UClass_ARubeGoldbergCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_RubeGoldberg();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	RUBEGOLDBERG_API UClass* Z_Construct_UClass_URubeGoldBergHUD_NoRegister();
// End Cross Module References
	void ARubeGoldbergCharacter::StaticRegisterNativesARubeGoldbergCharacter()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ARubeGoldbergCharacter);
	UClass* Z_Construct_UClass_ARubeGoldbergCharacter_NoRegister()
	{
		return ARubeGoldbergCharacter::StaticClass();
	}
	struct Z_Construct_UClass_ARubeGoldbergCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FollowCamera_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_FollowCamera;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TurnRateGamepad_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_TurnRateGamepad;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_GameHUDClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_GameHUDClass;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_GameHUD_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_GameHUD;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARubeGoldbergCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_RubeGoldberg,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARubeGoldbergCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "RubeGoldbergCharacter.h" },
		{ "ModuleRelativePath", "RubeGoldbergCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Camera boom positioning the camera behind the character */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "RubeGoldbergCharacter.h" },
		{ "ToolTip", "Camera boom positioning the camera behind the character" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_CameraBoom = { "CameraBoom", nullptr, (EPropertyFlags)0x00400000000a001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARubeGoldbergCharacter, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_CameraBoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_CameraBoom_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_FollowCamera_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Follow camera */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "RubeGoldbergCharacter.h" },
		{ "ToolTip", "Follow camera" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_FollowCamera = { "FollowCamera", nullptr, (EPropertyFlags)0x00400000000a001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARubeGoldbergCharacter, FollowCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_FollowCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_FollowCamera_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_TurnRateGamepad_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */" },
		{ "ModuleRelativePath", "RubeGoldbergCharacter.h" },
		{ "ToolTip", "Base turn rate, in deg/sec. Other scaling may affect final turn rate." },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_TurnRateGamepad = { "TurnRateGamepad", nullptr, (EPropertyFlags)0x0010000000020015, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARubeGoldbergCharacter, TurnRateGamepad), METADATA_PARAMS(Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_TurnRateGamepad_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_TurnRateGamepad_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_GameHUDClass_MetaData[] = {
		{ "Category", "RubeGoldbergCharacter" },
		{ "ModuleRelativePath", "RubeGoldbergCharacter.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_GameHUDClass = { "GameHUDClass", nullptr, (EPropertyFlags)0x0014000000000001, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARubeGoldbergCharacter, GameHUDClass), Z_Construct_UClass_URubeGoldBergHUD_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_GameHUDClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_GameHUDClass_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_GameHUD_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "RubeGoldbergCharacter.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_GameHUD = { "GameHUD", nullptr, (EPropertyFlags)0x0010000000080008, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARubeGoldbergCharacter, GameHUD), Z_Construct_UClass_URubeGoldBergHUD_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_GameHUD_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_GameHUD_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARubeGoldbergCharacter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_CameraBoom,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_FollowCamera,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_TurnRateGamepad,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_GameHUDClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARubeGoldbergCharacter_Statics::NewProp_GameHUD,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARubeGoldbergCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARubeGoldbergCharacter>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ARubeGoldbergCharacter_Statics::ClassParams = {
		&ARubeGoldbergCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ARubeGoldbergCharacter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ARubeGoldbergCharacter_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ARubeGoldbergCharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ARubeGoldbergCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARubeGoldbergCharacter()
	{
		if (!Z_Registration_Info_UClass_ARubeGoldbergCharacter.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ARubeGoldbergCharacter.OuterSingleton, Z_Construct_UClass_ARubeGoldbergCharacter_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ARubeGoldbergCharacter.OuterSingleton;
	}
	template<> RUBEGOLDBERG_API UClass* StaticClass<ARubeGoldbergCharacter>()
	{
		return ARubeGoldbergCharacter::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARubeGoldbergCharacter);
	struct Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergCharacter_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergCharacter_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ARubeGoldbergCharacter, ARubeGoldbergCharacter::StaticClass, TEXT("ARubeGoldbergCharacter"), &Z_Registration_Info_UClass_ARubeGoldbergCharacter, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ARubeGoldbergCharacter), 697311150U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergCharacter_h_937945706(TEXT("/Script/RubeGoldberg"),
		Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergCharacter_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergCharacter_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
