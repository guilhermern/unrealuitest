// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RubeGoldberg/RecipeMachine.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRecipeMachine() {}
// Cross Module References
	RUBEGOLDBERG_API UClass* Z_Construct_UClass_ARecipeMachine_NoRegister();
	RUBEGOLDBERG_API UClass* Z_Construct_UClass_ARecipeMachine();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_RubeGoldberg();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ARecipeMachine::execCheckRecipes)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CheckRecipes();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ARecipeMachine::execOnOverlapStarted)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent);
		P_GET_OBJECT(AActor,Z_Param_Other);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherIndex);
		P_GET_UBOOL(Z_Param_Sweep);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnOverlapStarted(Z_Param_OverlappedComponent,Z_Param_Other,Z_Param_OtherComp,Z_Param_OtherIndex,Z_Param_Sweep,Z_Param_Out_HitResult);
		P_NATIVE_END;
	}
	void ARecipeMachine::StaticRegisterNativesARecipeMachine()
	{
		UClass* Class = ARecipeMachine::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CheckRecipes", &ARecipeMachine::execCheckRecipes },
			{ "OnOverlapStarted", &ARecipeMachine::execOnOverlapStarted },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ARecipeMachine_CheckRecipes_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARecipeMachine_CheckRecipes_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "RecipeMachine.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ARecipeMachine_CheckRecipes_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARecipeMachine, nullptr, "CheckRecipes", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARecipeMachine_CheckRecipes_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ARecipeMachine_CheckRecipes_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARecipeMachine_CheckRecipes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ARecipeMachine_CheckRecipes_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics
	{
		struct RecipeMachine_eventOnOverlapStarted_Parms
		{
			UPrimitiveComponent* OverlappedComponent;
			AActor* Other;
			UPrimitiveComponent* OtherComp;
			int32 OtherIndex;
			bool Sweep;
			FHitResult HitResult;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OverlappedComponent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OverlappedComponent;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Other;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OtherComp_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OtherComp;
		static const UECodeGen_Private::FIntPropertyParams NewProp_OtherIndex;
		static void NewProp_Sweep_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Sweep;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_HitResult_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_HitResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_OverlappedComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_OverlappedComponent = { "OverlappedComponent", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RecipeMachine_eventOnOverlapStarted_Parms, OverlappedComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_OverlappedComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_OverlappedComponent_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_Other = { "Other", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RecipeMachine_eventOnOverlapStarted_Parms, Other), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_OtherComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_OtherComp = { "OtherComp", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RecipeMachine_eventOnOverlapStarted_Parms, OtherComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_OtherComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_OtherComp_MetaData)) };
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_OtherIndex = { "OtherIndex", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RecipeMachine_eventOnOverlapStarted_Parms, OtherIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_Sweep_SetBit(void* Obj)
	{
		((RecipeMachine_eventOnOverlapStarted_Parms*)Obj)->Sweep = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_Sweep = { "Sweep", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(RecipeMachine_eventOnOverlapStarted_Parms), &Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_Sweep_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_HitResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RecipeMachine_eventOnOverlapStarted_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_HitResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_HitResult_MetaData)) }; // 1416937132
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_OverlappedComponent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_Other,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_OtherComp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_OtherIndex,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_Sweep,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::NewProp_HitResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "RecipeMachine.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARecipeMachine, nullptr, "OnOverlapStarted", nullptr, nullptr, sizeof(Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::RecipeMachine_eventOnOverlapStarted_Parms), Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00480401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ARecipeMachine);
	UClass* Z_Construct_UClass_ARecipeMachine_NoRegister()
	{
		return ARecipeMachine::StaticClass();
	}
	struct Z_Construct_UClass_ARecipeMachine_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Mesh_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Mesh;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CollisionBox_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_CollisionBox;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ResultSpawnPosition_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ResultSpawnPosition;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Recipes_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Recipes;
		static const UECodeGen_Private::FNamePropertyParams NewProp_RecipeNames_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RecipeNames_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_RecipeNames;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARecipeMachine_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_RubeGoldberg,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ARecipeMachine_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ARecipeMachine_CheckRecipes, "CheckRecipes" }, // 2524477746
		{ &Z_Construct_UFunction_ARecipeMachine_OnOverlapStarted, "OnOverlapStarted" }, // 62857248
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARecipeMachine_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "RecipeMachine.h" },
		{ "ModuleRelativePath", "RecipeMachine.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARecipeMachine_Statics::NewProp_Mesh_MetaData[] = {
		{ "Category", "RecipeMachine" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "RecipeMachine.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARecipeMachine_Statics::NewProp_Mesh = { "Mesh", nullptr, (EPropertyFlags)0x002008000008000d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARecipeMachine, Mesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARecipeMachine_Statics::NewProp_Mesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARecipeMachine_Statics::NewProp_Mesh_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARecipeMachine_Statics::NewProp_CollisionBox_MetaData[] = {
		{ "Category", "RecipeMachine" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "RecipeMachine.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARecipeMachine_Statics::NewProp_CollisionBox = { "CollisionBox", nullptr, (EPropertyFlags)0x002008000008000d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARecipeMachine, CollisionBox), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARecipeMachine_Statics::NewProp_CollisionBox_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARecipeMachine_Statics::NewProp_CollisionBox_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARecipeMachine_Statics::NewProp_ResultSpawnPosition_MetaData[] = {
		{ "Category", "RecipeMachine" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "RecipeMachine.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARecipeMachine_Statics::NewProp_ResultSpawnPosition = { "ResultSpawnPosition", nullptr, (EPropertyFlags)0x002008000008000d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARecipeMachine, ResultSpawnPosition), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARecipeMachine_Statics::NewProp_ResultSpawnPosition_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARecipeMachine_Statics::NewProp_ResultSpawnPosition_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARecipeMachine_Statics::NewProp_Recipes_MetaData[] = {
		{ "Category", "RecipeMachine" },
		{ "ModuleRelativePath", "RecipeMachine.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARecipeMachine_Statics::NewProp_Recipes = { "Recipes", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARecipeMachine, Recipes), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARecipeMachine_Statics::NewProp_Recipes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARecipeMachine_Statics::NewProp_Recipes_MetaData)) };
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UClass_ARecipeMachine_Statics::NewProp_RecipeNames_Inner = { "RecipeNames", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARecipeMachine_Statics::NewProp_RecipeNames_MetaData[] = {
		{ "Category", "RecipeMachine" },
		{ "ModuleRelativePath", "RecipeMachine.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ARecipeMachine_Statics::NewProp_RecipeNames = { "RecipeNames", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARecipeMachine, RecipeNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ARecipeMachine_Statics::NewProp_RecipeNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARecipeMachine_Statics::NewProp_RecipeNames_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARecipeMachine_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARecipeMachine_Statics::NewProp_Mesh,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARecipeMachine_Statics::NewProp_CollisionBox,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARecipeMachine_Statics::NewProp_ResultSpawnPosition,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARecipeMachine_Statics::NewProp_Recipes,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARecipeMachine_Statics::NewProp_RecipeNames_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARecipeMachine_Statics::NewProp_RecipeNames,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARecipeMachine_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARecipeMachine>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ARecipeMachine_Statics::ClassParams = {
		&ARecipeMachine::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ARecipeMachine_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ARecipeMachine_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ARecipeMachine_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ARecipeMachine_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARecipeMachine()
	{
		if (!Z_Registration_Info_UClass_ARecipeMachine.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ARecipeMachine.OuterSingleton, Z_Construct_UClass_ARecipeMachine_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ARecipeMachine.OuterSingleton;
	}
	template<> RUBEGOLDBERG_API UClass* StaticClass<ARecipeMachine>()
	{
		return ARecipeMachine::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARecipeMachine);
	struct Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ARecipeMachine, ARecipeMachine::StaticClass, TEXT("ARecipeMachine"), &Z_Registration_Info_UClass_ARecipeMachine, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ARecipeMachine), 3051947403U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_1087994558(TEXT("/Script/RubeGoldberg"),
		Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
