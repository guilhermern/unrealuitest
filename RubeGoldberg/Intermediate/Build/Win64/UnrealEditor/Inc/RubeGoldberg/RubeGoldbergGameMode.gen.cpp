// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RubeGoldberg/RubeGoldbergGameMode.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRubeGoldbergGameMode() {}
// Cross Module References
	RUBEGOLDBERG_API UClass* Z_Construct_UClass_ARubeGoldbergGameMode_NoRegister();
	RUBEGOLDBERG_API UClass* Z_Construct_UClass_ARubeGoldbergGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_RubeGoldberg();
// End Cross Module References
	void ARubeGoldbergGameMode::StaticRegisterNativesARubeGoldbergGameMode()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ARubeGoldbergGameMode);
	UClass* Z_Construct_UClass_ARubeGoldbergGameMode_NoRegister()
	{
		return ARubeGoldbergGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ARubeGoldbergGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARubeGoldbergGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_RubeGoldberg,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARubeGoldbergGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "RubeGoldbergGameMode.h" },
		{ "ModuleRelativePath", "RubeGoldbergGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARubeGoldbergGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARubeGoldbergGameMode>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ARubeGoldbergGameMode_Statics::ClassParams = {
		&ARubeGoldbergGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_ARubeGoldbergGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ARubeGoldbergGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARubeGoldbergGameMode()
	{
		if (!Z_Registration_Info_UClass_ARubeGoldbergGameMode.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ARubeGoldbergGameMode.OuterSingleton, Z_Construct_UClass_ARubeGoldbergGameMode_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ARubeGoldbergGameMode.OuterSingleton;
	}
	template<> RUBEGOLDBERG_API UClass* StaticClass<ARubeGoldbergGameMode>()
	{
		return ARubeGoldbergGameMode::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARubeGoldbergGameMode);
	struct Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ARubeGoldbergGameMode, ARubeGoldbergGameMode::StaticClass, TEXT("ARubeGoldbergGameMode"), &Z_Registration_Info_UClass_ARubeGoldbergGameMode, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ARubeGoldbergGameMode), 155534871U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_1932417811(TEXT("/Script/RubeGoldberg"),
		Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
