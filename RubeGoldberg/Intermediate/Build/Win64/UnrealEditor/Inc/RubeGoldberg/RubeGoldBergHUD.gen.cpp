// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RubeGoldberg/RubeGoldBergHUD.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRubeGoldBergHUD() {}
// Cross Module References
	RUBEGOLDBERG_API UClass* Z_Construct_UClass_URubeGoldBergHUD_NoRegister();
	RUBEGOLDBERG_API UClass* Z_Construct_UClass_URubeGoldBergHUD();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_RubeGoldberg();
	SLATECORE_API UEnum* Z_Construct_UEnum_SlateCore_ESelectInfo();
	RUBEGOLDBERG_API UClass* Z_Construct_UClass_ARecipeMachine_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UComboBoxString_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UListView_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(URubeGoldBergHUD::execOnFKeyPressed)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnFKeyPressed();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URubeGoldBergHUD::execOnComboBoxSelectionChanged)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Item);
		P_GET_PROPERTY(FByteProperty,Z_Param_SelectionType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnComboBoxSelectionChanged(Z_Param_Item,ESelectInfo::Type(Z_Param_SelectionType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URubeGoldBergHUD::execOnEnabledCheckBoxChanged)
	{
		P_GET_UBOOL(Z_Param_NewState);
		P_GET_PROPERTY(FIntProperty,Z_Param_RecipeIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnEnabledCheckBoxChanged(Z_Param_NewState,Z_Param_RecipeIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URubeGoldBergHUD::execOnSpawnButtonPressed)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_RecipeIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnSpawnButtonPressed(Z_Param_RecipeIndex);
		P_NATIVE_END;
	}
	void URubeGoldBergHUD::StaticRegisterNativesURubeGoldBergHUD()
	{
		UClass* Class = URubeGoldBergHUD::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnComboBoxSelectionChanged", &URubeGoldBergHUD::execOnComboBoxSelectionChanged },
			{ "OnEnabledCheckBoxChanged", &URubeGoldBergHUD::execOnEnabledCheckBoxChanged },
			{ "OnFKeyPressed", &URubeGoldBergHUD::execOnFKeyPressed },
			{ "OnSpawnButtonPressed", &URubeGoldBergHUD::execOnSpawnButtonPressed },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics
	{
		struct RubeGoldBergHUD_eventOnComboBoxSelectionChanged_Parms
		{
			FString Item;
			TEnumAsByte<ESelectInfo::Type> SelectionType;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_Item;
		static const UECodeGen_Private::FBytePropertyParams NewProp_SelectionType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics::NewProp_Item = { "Item", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RubeGoldBergHUD_eventOnComboBoxSelectionChanged_Parms, Item), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics::NewProp_SelectionType = { "SelectionType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RubeGoldBergHUD_eventOnComboBoxSelectionChanged_Parms, SelectionType), Z_Construct_UEnum_SlateCore_ESelectInfo, METADATA_PARAMS(nullptr, 0) }; // 2646121829
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics::NewProp_Item,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics::NewProp_SelectionType,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "RubeGoldBergHUD.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URubeGoldBergHUD, nullptr, "OnComboBoxSelectionChanged", nullptr, nullptr, sizeof(Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics::RubeGoldBergHUD_eventOnComboBoxSelectionChanged_Parms), Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics
	{
		struct RubeGoldBergHUD_eventOnEnabledCheckBoxChanged_Parms
		{
			bool NewState;
			int32 RecipeIndex;
		};
		static void NewProp_NewState_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_NewState;
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_RecipeIndex;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::NewProp_NewState_SetBit(void* Obj)
	{
		((RubeGoldBergHUD_eventOnEnabledCheckBoxChanged_Parms*)Obj)->NewState = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::NewProp_NewState = { "NewState", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(RubeGoldBergHUD_eventOnEnabledCheckBoxChanged_Parms), &Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::NewProp_NewState_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::NewProp_RecipeIndex = { "RecipeIndex", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RubeGoldBergHUD_eventOnEnabledCheckBoxChanged_Parms, RecipeIndex), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::NewProp_NewState,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::NewProp_RecipeIndex,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "RubeGoldBergHUD.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URubeGoldBergHUD, nullptr, "OnEnabledCheckBoxChanged", nullptr, nullptr, sizeof(Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::RubeGoldBergHUD_eventOnEnabledCheckBoxChanged_Parms), Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URubeGoldBergHUD_OnFKeyPressed_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URubeGoldBergHUD_OnFKeyPressed_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "RubeGoldBergHUD.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URubeGoldBergHUD_OnFKeyPressed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URubeGoldBergHUD, nullptr, "OnFKeyPressed", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URubeGoldBergHUD_OnFKeyPressed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URubeGoldBergHUD_OnFKeyPressed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URubeGoldBergHUD_OnFKeyPressed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URubeGoldBergHUD_OnFKeyPressed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed_Statics
	{
		struct RubeGoldBergHUD_eventOnSpawnButtonPressed_Parms
		{
			int32 RecipeIndex;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_RecipeIndex;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed_Statics::NewProp_RecipeIndex = { "RecipeIndex", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RubeGoldBergHUD_eventOnSpawnButtonPressed_Parms, RecipeIndex), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed_Statics::NewProp_RecipeIndex,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "RubeGoldBergHUD.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URubeGoldBergHUD, nullptr, "OnSpawnButtonPressed", nullptr, nullptr, sizeof(Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed_Statics::RubeGoldBergHUD_eventOnSpawnButtonPressed_Parms), Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(URubeGoldBergHUD);
	UClass* Z_Construct_UClass_URubeGoldBergHUD_NoRegister()
	{
		return URubeGoldBergHUD::StaticClass();
	}
	struct Z_Construct_UClass_URubeGoldBergHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ComboBoxOptions_ValueProp;
		static const UECodeGen_Private::FIntPropertyParams NewProp_ComboBoxOptions_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ComboBoxOptions_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_ComboBoxOptions;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ComboBox_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ComboBox;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RecipesListView_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_RecipesListView;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RecipeMachineClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_RecipeMachineClass;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URubeGoldBergHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_RubeGoldberg,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_URubeGoldBergHUD_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_URubeGoldBergHUD_OnComboBoxSelectionChanged, "OnComboBoxSelectionChanged" }, // 2824623549
		{ &Z_Construct_UFunction_URubeGoldBergHUD_OnEnabledCheckBoxChanged, "OnEnabledCheckBoxChanged" }, // 3754531509
		{ &Z_Construct_UFunction_URubeGoldBergHUD_OnFKeyPressed, "OnFKeyPressed" }, // 1654910050
		{ &Z_Construct_UFunction_URubeGoldBergHUD_OnSpawnButtonPressed, "OnSpawnButtonPressed" }, // 1337875135
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URubeGoldBergHUD_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "RubeGoldBergHUD.h" },
		{ "ModuleRelativePath", "RubeGoldBergHUD.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBoxOptions_ValueProp = { "ComboBoxOptions", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_ARecipeMachine_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBoxOptions_Key_KeyProp = { "ComboBoxOptions_Key", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBoxOptions_MetaData[] = {
		{ "Category", "RubeGoldBergHUD" },
		{ "ModuleRelativePath", "RubeGoldBergHUD.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBoxOptions = { "ComboBoxOptions", nullptr, (EPropertyFlags)0x0040000000000001, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URubeGoldBergHUD, ComboBoxOptions), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBoxOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBoxOptions_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBox_MetaData[] = {
		{ "BindWidget", "" },
		{ "Category", "RubeGoldBergHUD" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "RubeGoldBergHUD.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBox = { "ComboBox", nullptr, (EPropertyFlags)0x0010000000080009, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URubeGoldBergHUD, ComboBox), Z_Construct_UClass_UComboBoxString_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBox_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBox_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_RecipesListView_MetaData[] = {
		{ "BindWidget", "" },
		{ "Category", "RubeGoldBergHUD" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "RubeGoldBergHUD.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_RecipesListView = { "RecipesListView", nullptr, (EPropertyFlags)0x0010000000080009, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URubeGoldBergHUD, RecipesListView), Z_Construct_UClass_UListView_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_RecipesListView_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_RecipesListView_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_RecipeMachineClass_MetaData[] = {
		{ "Category", "RubeGoldBergHUD" },
		{ "ModuleRelativePath", "RubeGoldBergHUD.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_RecipeMachineClass = { "RecipeMachineClass", nullptr, (EPropertyFlags)0x0014000000000001, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URubeGoldBergHUD, RecipeMachineClass), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_RecipeMachineClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_RecipeMachineClass_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URubeGoldBergHUD_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBoxOptions_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBoxOptions_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBoxOptions,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_ComboBox,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_RecipesListView,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URubeGoldBergHUD_Statics::NewProp_RecipeMachineClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URubeGoldBergHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URubeGoldBergHUD>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_URubeGoldBergHUD_Statics::ClassParams = {
		&URubeGoldBergHUD::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_URubeGoldBergHUD_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_URubeGoldBergHUD_Statics::PropPointers),
		0,
		0x00B010A1u,
		METADATA_PARAMS(Z_Construct_UClass_URubeGoldBergHUD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URubeGoldBergHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URubeGoldBergHUD()
	{
		if (!Z_Registration_Info_UClass_URubeGoldBergHUD.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_URubeGoldBergHUD.OuterSingleton, Z_Construct_UClass_URubeGoldBergHUD_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_URubeGoldBergHUD.OuterSingleton;
	}
	template<> RUBEGOLDBERG_API UClass* StaticClass<URubeGoldBergHUD>()
	{
		return URubeGoldBergHUD::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(URubeGoldBergHUD);
	struct Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldBergHUD_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldBergHUD_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_URubeGoldBergHUD, URubeGoldBergHUD::StaticClass, TEXT("URubeGoldBergHUD"), &Z_Registration_Info_UClass_URubeGoldBergHUD, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(URubeGoldBergHUD), 220198035U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldBergHUD_h_546739904(TEXT("/Script/RubeGoldberg"),
		Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldBergHUD_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldBergHUD_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
