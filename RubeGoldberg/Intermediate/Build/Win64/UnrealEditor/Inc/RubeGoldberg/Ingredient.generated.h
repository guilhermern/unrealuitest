// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RUBEGOLDBERG_Ingredient_generated_h
#error "Ingredient.generated.h already included, missing '#pragma once' in Ingredient.h"
#endif
#define RUBEGOLDBERG_Ingredient_generated_h

#define FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_SPARSE_DATA
#define FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_RPC_WRAPPERS
#define FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAIngredient(); \
	friend struct Z_Construct_UClass_AIngredient_Statics; \
public: \
	DECLARE_CLASS(AIngredient, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/RubeGoldberg"), NO_API) \
	DECLARE_SERIALIZER(AIngredient)


#define FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAIngredient(); \
	friend struct Z_Construct_UClass_AIngredient_Statics; \
public: \
	DECLARE_CLASS(AIngredient, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/RubeGoldberg"), NO_API) \
	DECLARE_SERIALIZER(AIngredient)


#define FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AIngredient(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AIngredient) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIngredient); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIngredient); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIngredient(AIngredient&&); \
	NO_API AIngredient(const AIngredient&); \
public:


#define FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AIngredient(AIngredient&&); \
	NO_API AIngredient(const AIngredient&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AIngredient); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AIngredient); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AIngredient)


#define FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_9_PROLOG
#define FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_SPARSE_DATA \
	FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_RPC_WRAPPERS \
	FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_INCLASS \
	FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_SPARSE_DATA \
	FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_INCLASS_NO_PURE_DECLS \
	FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RUBEGOLDBERG_API UClass* StaticClass<class AIngredient>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_RubeGoldberg_Source_RubeGoldberg_Ingredient_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
