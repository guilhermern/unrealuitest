// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RUBEGOLDBERG_FRecipeItem_generated_h
#error "FRecipeItem.generated.h already included, missing '#pragma once' in FRecipeItem.h"
#endif
#define RUBEGOLDBERG_FRecipeItem_generated_h

#define FID_RubeGoldberg_Source_RubeGoldberg_FRecipeItem_h_9_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRecipeItem_Statics; \
	RUBEGOLDBERG_API static class UScriptStruct* StaticStruct();


template<> RUBEGOLDBERG_API UScriptStruct* StaticStruct<struct FRecipeItem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_RubeGoldberg_Source_RubeGoldberg_FRecipeItem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
