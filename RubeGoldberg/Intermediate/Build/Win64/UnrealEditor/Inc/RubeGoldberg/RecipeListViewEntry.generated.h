// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RUBEGOLDBERG_RecipeListViewEntry_generated_h
#error "RecipeListViewEntry.generated.h already included, missing '#pragma once' in RecipeListViewEntry.h"
#endif
#define RUBEGOLDBERG_RecipeListViewEntry_generated_h

#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_12_DELEGATE \
struct _Script_RubeGoldberg_eventOnSpawnButtonPressedSignature_Parms \
{ \
	int32 RecipeIndex; \
}; \
static inline void FOnSpawnButtonPressedSignature_DelegateWrapper(const FMulticastScriptDelegate& OnSpawnButtonPressedSignature, int32 RecipeIndex) \
{ \
	_Script_RubeGoldberg_eventOnSpawnButtonPressedSignature_Parms Parms; \
	Parms.RecipeIndex=RecipeIndex; \
	OnSpawnButtonPressedSignature.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_13_DELEGATE \
struct _Script_RubeGoldberg_eventOnEnabledCheckboxChangedSignature_Parms \
{ \
	bool NewState; \
	int32 RecipeIndex; \
}; \
static inline void FOnEnabledCheckboxChangedSignature_DelegateWrapper(const FMulticastScriptDelegate& OnEnabledCheckboxChangedSignature, bool NewState, int32 RecipeIndex) \
{ \
	_Script_RubeGoldberg_eventOnEnabledCheckboxChangedSignature_Parms Parms; \
	Parms.NewState=NewState ? true : false; \
	Parms.RecipeIndex=RecipeIndex; \
	OnEnabledCheckboxChangedSignature.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_SPARSE_DATA
#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnCheckboxChangedState); \
	DECLARE_FUNCTION(execOnSpawnButtonPressed);


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnCheckboxChangedState); \
	DECLARE_FUNCTION(execOnSpawnButtonPressed);


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURecipeListViewEntry(); \
	friend struct Z_Construct_UClass_URecipeListViewEntry_Statics; \
public: \
	DECLARE_CLASS(URecipeListViewEntry, UUserWidget, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/RubeGoldberg"), NO_API) \
	DECLARE_SERIALIZER(URecipeListViewEntry) \
	virtual UObject* _getUObject() const override { return const_cast<URecipeListViewEntry*>(this); }


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_INCLASS \
private: \
	static void StaticRegisterNativesURecipeListViewEntry(); \
	friend struct Z_Construct_UClass_URecipeListViewEntry_Statics; \
public: \
	DECLARE_CLASS(URecipeListViewEntry, UUserWidget, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/RubeGoldberg"), NO_API) \
	DECLARE_SERIALIZER(URecipeListViewEntry) \
	virtual UObject* _getUObject() const override { return const_cast<URecipeListViewEntry*>(this); }


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URecipeListViewEntry(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URecipeListViewEntry) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URecipeListViewEntry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URecipeListViewEntry); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URecipeListViewEntry(URecipeListViewEntry&&); \
	NO_API URecipeListViewEntry(const URecipeListViewEntry&); \
public:


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URecipeListViewEntry(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URecipeListViewEntry(URecipeListViewEntry&&); \
	NO_API URecipeListViewEntry(const URecipeListViewEntry&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URecipeListViewEntry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URecipeListViewEntry); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URecipeListViewEntry)


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_15_PROLOG
#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_SPARSE_DATA \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_RPC_WRAPPERS \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_INCLASS \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_SPARSE_DATA \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_INCLASS_NO_PURE_DECLS \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RUBEGOLDBERG_API UClass* StaticClass<class URecipeListViewEntry>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewEntry_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
