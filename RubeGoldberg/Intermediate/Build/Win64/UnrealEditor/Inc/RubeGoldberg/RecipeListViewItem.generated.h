// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RUBEGOLDBERG_RecipeListViewItem_generated_h
#error "RecipeListViewItem.generated.h already included, missing '#pragma once' in RecipeListViewItem.h"
#endif
#define RUBEGOLDBERG_RecipeListViewItem_generated_h

#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_8_DELEGATE \
struct _Script_RubeGoldberg_eventOnSpawnButtonPressedSig_Parms \
{ \
	int32 RecipeIndex; \
}; \
static inline void FOnSpawnButtonPressedSig_DelegateWrapper(const FMulticastScriptDelegate& OnSpawnButtonPressedSig, int32 RecipeIndex) \
{ \
	_Script_RubeGoldberg_eventOnSpawnButtonPressedSig_Parms Parms; \
	Parms.RecipeIndex=RecipeIndex; \
	OnSpawnButtonPressedSig.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_9_DELEGATE \
struct _Script_RubeGoldberg_eventOnEnabledCheckboxChangedSig_Parms \
{ \
	bool NewState; \
	int32 RecipeIndex; \
}; \
static inline void FOnEnabledCheckboxChangedSig_DelegateWrapper(const FMulticastScriptDelegate& OnEnabledCheckboxChangedSig, bool NewState, int32 RecipeIndex) \
{ \
	_Script_RubeGoldberg_eventOnEnabledCheckboxChangedSig_Parms Parms; \
	Parms.NewState=NewState ? true : false; \
	Parms.RecipeIndex=RecipeIndex; \
	OnEnabledCheckboxChangedSig.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_SPARSE_DATA
#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnCheckboxStateChanged); \
	DECLARE_FUNCTION(execOnSpawnButtonPressed);


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnCheckboxStateChanged); \
	DECLARE_FUNCTION(execOnSpawnButtonPressed);


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURecipeListViewItem(); \
	friend struct Z_Construct_UClass_URecipeListViewItem_Statics; \
public: \
	DECLARE_CLASS(URecipeListViewItem, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RubeGoldberg"), NO_API) \
	DECLARE_SERIALIZER(URecipeListViewItem)


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_INCLASS \
private: \
	static void StaticRegisterNativesURecipeListViewItem(); \
	friend struct Z_Construct_UClass_URecipeListViewItem_Statics; \
public: \
	DECLARE_CLASS(URecipeListViewItem, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RubeGoldberg"), NO_API) \
	DECLARE_SERIALIZER(URecipeListViewItem)


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URecipeListViewItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URecipeListViewItem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URecipeListViewItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URecipeListViewItem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URecipeListViewItem(URecipeListViewItem&&); \
	NO_API URecipeListViewItem(const URecipeListViewItem&); \
public:


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URecipeListViewItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URecipeListViewItem(URecipeListViewItem&&); \
	NO_API URecipeListViewItem(const URecipeListViewItem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URecipeListViewItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URecipeListViewItem); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URecipeListViewItem)


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_11_PROLOG
#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_SPARSE_DATA \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_RPC_WRAPPERS \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_INCLASS \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_SPARSE_DATA \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_INCLASS_NO_PURE_DECLS \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RUBEGOLDBERG_API UClass* StaticClass<class URecipeListViewItem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_RubeGoldberg_Source_RubeGoldberg_RecipeListViewItem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
