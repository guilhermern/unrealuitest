// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef RUBEGOLDBERG_RecipeMachine_generated_h
#error "RecipeMachine.generated.h already included, missing '#pragma once' in RecipeMachine.h"
#endif
#define RUBEGOLDBERG_RecipeMachine_generated_h

#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_SPARSE_DATA
#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCheckRecipes); \
	DECLARE_FUNCTION(execOnOverlapStarted);


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCheckRecipes); \
	DECLARE_FUNCTION(execOnOverlapStarted);


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARecipeMachine(); \
	friend struct Z_Construct_UClass_ARecipeMachine_Statics; \
public: \
	DECLARE_CLASS(ARecipeMachine, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/RubeGoldberg"), NO_API) \
	DECLARE_SERIALIZER(ARecipeMachine)


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_INCLASS \
private: \
	static void StaticRegisterNativesARecipeMachine(); \
	friend struct Z_Construct_UClass_ARecipeMachine_Statics; \
public: \
	DECLARE_CLASS(ARecipeMachine, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/RubeGoldberg"), NO_API) \
	DECLARE_SERIALIZER(ARecipeMachine)


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARecipeMachine(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARecipeMachine) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARecipeMachine); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARecipeMachine); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARecipeMachine(ARecipeMachine&&); \
	NO_API ARecipeMachine(const ARecipeMachine&); \
public:


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARecipeMachine(ARecipeMachine&&); \
	NO_API ARecipeMachine(const ARecipeMachine&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARecipeMachine); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARecipeMachine); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARecipeMachine)


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_14_PROLOG
#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_SPARSE_DATA \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_RPC_WRAPPERS \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_INCLASS \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_SPARSE_DATA \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_INCLASS_NO_PURE_DECLS \
	FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RUBEGOLDBERG_API UClass* StaticClass<class ARecipeMachine>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_RubeGoldberg_Source_RubeGoldberg_RecipeMachine_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
