// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RubeGoldberg/FRecipeItem.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFRecipeItem() {}
// Cross Module References
	RUBEGOLDBERG_API UScriptStruct* Z_Construct_UScriptStruct_FRecipeItem();
	UPackage* Z_Construct_UPackage__Script_RubeGoldberg();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	RUBEGOLDBERG_API UClass* Z_Construct_UClass_AIngredient_NoRegister();
// End Cross Module References
	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_RecipeItem;
class UScriptStruct* FRecipeItem::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_RecipeItem.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_RecipeItem.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FRecipeItem, Z_Construct_UPackage__Script_RubeGoldberg(), TEXT("RecipeItem"));
	}
	return Z_Registration_Info_UScriptStruct_RecipeItem.OuterSingleton;
}
template<> RUBEGOLDBERG_API UScriptStruct* StaticStruct<FRecipeItem>()
{
	return FRecipeItem::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FRecipeItem_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Item_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_Item;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Quantity_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_Quantity;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRecipeItem_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "FRecipeItem.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRecipeItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRecipeItem>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRecipeItem_Statics::NewProp_Item_MetaData[] = {
		{ "Category", "RecipeItem" },
		{ "ModuleRelativePath", "FRecipeItem.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FRecipeItem_Statics::NewProp_Item = { "Item", nullptr, (EPropertyFlags)0x0014000000000005, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRecipeItem, Item), Z_Construct_UClass_AIngredient_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FRecipeItem_Statics::NewProp_Item_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRecipeItem_Statics::NewProp_Item_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRecipeItem_Statics::NewProp_Quantity_MetaData[] = {
		{ "Category", "RecipeItem" },
		{ "ModuleRelativePath", "FRecipeItem.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FRecipeItem_Statics::NewProp_Quantity = { "Quantity", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRecipeItem, Quantity), METADATA_PARAMS(Z_Construct_UScriptStruct_FRecipeItem_Statics::NewProp_Quantity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRecipeItem_Statics::NewProp_Quantity_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRecipeItem_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRecipeItem_Statics::NewProp_Item,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRecipeItem_Statics::NewProp_Quantity,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRecipeItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RubeGoldberg,
		nullptr,
		&NewStructOps,
		"RecipeItem",
		sizeof(FRecipeItem),
		alignof(FRecipeItem),
		Z_Construct_UScriptStruct_FRecipeItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRecipeItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRecipeItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRecipeItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRecipeItem()
	{
		if (!Z_Registration_Info_UScriptStruct_RecipeItem.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_RecipeItem.InnerSingleton, Z_Construct_UScriptStruct_FRecipeItem_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_RecipeItem.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_FRecipeItem_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_FRecipeItem_h_Statics::ScriptStructInfo[] = {
		{ FRecipeItem::StaticStruct, Z_Construct_UScriptStruct_FRecipeItem_Statics::NewStructOps, TEXT("RecipeItem"), &Z_Registration_Info_UScriptStruct_RecipeItem, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FRecipeItem), 3891100002U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_FRecipeItem_h_3632519176(TEXT("/Script/RubeGoldberg"),
		nullptr, 0,
		Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_FRecipeItem_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_RubeGoldberg_Source_RubeGoldberg_FRecipeItem_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
