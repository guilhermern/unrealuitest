// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RUBEGOLDBERG_RubeGoldbergGameMode_generated_h
#error "RubeGoldbergGameMode.generated.h already included, missing '#pragma once' in RubeGoldbergGameMode.h"
#endif
#define RUBEGOLDBERG_RubeGoldbergGameMode_generated_h

#define FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_SPARSE_DATA
#define FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_RPC_WRAPPERS
#define FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARubeGoldbergGameMode(); \
	friend struct Z_Construct_UClass_ARubeGoldbergGameMode_Statics; \
public: \
	DECLARE_CLASS(ARubeGoldbergGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/RubeGoldberg"), RUBEGOLDBERG_API) \
	DECLARE_SERIALIZER(ARubeGoldbergGameMode)


#define FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesARubeGoldbergGameMode(); \
	friend struct Z_Construct_UClass_ARubeGoldbergGameMode_Statics; \
public: \
	DECLARE_CLASS(ARubeGoldbergGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/RubeGoldberg"), RUBEGOLDBERG_API) \
	DECLARE_SERIALIZER(ARubeGoldbergGameMode)


#define FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	RUBEGOLDBERG_API ARubeGoldbergGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARubeGoldbergGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(RUBEGOLDBERG_API, ARubeGoldbergGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARubeGoldbergGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	RUBEGOLDBERG_API ARubeGoldbergGameMode(ARubeGoldbergGameMode&&); \
	RUBEGOLDBERG_API ARubeGoldbergGameMode(const ARubeGoldbergGameMode&); \
public:


#define FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	RUBEGOLDBERG_API ARubeGoldbergGameMode(ARubeGoldbergGameMode&&); \
	RUBEGOLDBERG_API ARubeGoldbergGameMode(const ARubeGoldbergGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(RUBEGOLDBERG_API, ARubeGoldbergGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARubeGoldbergGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARubeGoldbergGameMode)


#define FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_9_PROLOG
#define FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_SPARSE_DATA \
	FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_RPC_WRAPPERS \
	FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_INCLASS \
	FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_SPARSE_DATA \
	FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_INCLASS_NO_PURE_DECLS \
	FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RUBEGOLDBERG_API UClass* StaticClass<class ARubeGoldbergGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_RubeGoldberg_Source_RubeGoldberg_RubeGoldbergGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
