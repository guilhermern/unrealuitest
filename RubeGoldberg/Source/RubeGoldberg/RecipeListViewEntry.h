// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "Blueprint/UserWidget.h"
#include "Delegates/DelegateCombinations.h"
#include "Delegates/DelegateCombinations.h"
#include "RecipeListViewEntry.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSpawnButtonPressedSignature, int, RecipeIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnEnabledCheckboxChangedSignature, bool, NewState, int, RecipeIndex);

UCLASS(Abstract)
class RUBEGOLDBERG_API URecipeListViewEntry : public UUserWidget, public IUserObjectListEntry
{
private:
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintCallable)
	FOnSpawnButtonPressedSignature OnSpawnButtonPressedDelegate;

	UPROPERTY(BlueprintCallable)
	FOnEnabledCheckboxChangedSignature OnCheckboxChangedDelegate;
	
protected:
	virtual void NativeOnListItemObjectSet(UObject* ListItemObject) override;

	UFUNCTION()
	void OnSpawnButtonPressed();

	UFUNCTION()
	void OnCheckboxChangedState(bool NewState);

	UPROPERTY(meta=(BindWidget))
	class UTextBlock* TitleLabel;

	UPROPERTY(meta=(BindWidget))
	UTextBlock* IngredientsLabel;

	UPROPERTY(meta=(BindWidget))
	UTextBlock* OutputLabel;

	UPROPERTY(meta=(BindWidget))
	class UButton* SpawnResultButton;

	UPROPERTY(meta=(BindWidget))
	class UCheckBox* EnabledCheckBox;

private:
	int Index;
};
