// Copyright Epic Games, Inc. All Rights Reserved.

#include "RubeGoldbergGameMode.h"
#include "RubeGoldbergCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARubeGoldbergGameMode::ARubeGoldbergGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
