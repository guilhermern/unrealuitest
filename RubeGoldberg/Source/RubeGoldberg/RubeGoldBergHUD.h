// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "RubeGoldBergHUD.generated.h"

class ARecipeMachine;
class FSlateEnums;
class URecipeListViewItem;

UCLASS(Abstract)
class RUBEGOLDBERG_API URubeGoldBergHUD : public UUserWidget
{
	GENERATED_BODY()

protected:
	void PopulateComboBoxOptions();
	void SetupListViewCallbacks(TArray<URecipeListViewItem*> &ListViewEntriesItems, TArray<int>& DisabledRecipes);

	UFUNCTION()
	void OnSpawnButtonPressed(int RecipeIndex);

	UFUNCTION()
	void OnEnabledCheckBoxChanged(bool NewState, int RecipeIndex);
	
	UFUNCTION()
	void OnComboBoxSelectionChanged(FString Item, ESelectInfo::Type SelectionType);

private:
	UPROPERTY(EditAnywhere)
	TMap<int32, ARecipeMachine*> ComboBoxOptions;

	ESlateVisibility CurrentVisibility = ESlateVisibility::Hidden;

	int32 SelectedIndex;

public:

	virtual void NativeOnInitialized() override;
	
	UFUNCTION(BlueprintCallable)
	void OnFKeyPressed();

public:
	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UComboBoxString* ComboBox;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UListView* RecipesListView;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> RecipeMachineClass;
};
