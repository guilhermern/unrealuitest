// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RecipeListViewItem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSpawnButtonPressedSig, int, RecipeIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams (FOnEnabledCheckboxChangedSig, bool, NewState, int, RecipeIndex);

UCLASS()
class RUBEGOLDBERG_API URecipeListViewItem : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintCallable)
	FOnSpawnButtonPressedSig OnSpawnButtonPressedDelegate;

	UPROPERTY(BlueprintCallable)
	FOnEnabledCheckboxChangedSig OnCheckboxChangedDelegate;

	int Index;
	
	FString TitleLabel;
	FString IngredientsLabel;
	FString OutputLabel;
	bool RecipeDisabled;

	UFUNCTION()
	void OnSpawnButtonPressed(int RecipeIndex);

	UFUNCTION()
	void OnCheckboxStateChanged(bool NewState, int RecipeIndex);
};
