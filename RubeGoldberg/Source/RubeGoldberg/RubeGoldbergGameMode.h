// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RubeGoldbergGameMode.generated.h"

UCLASS(minimalapi)
class ARubeGoldbergGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARubeGoldbergGameMode();
};



