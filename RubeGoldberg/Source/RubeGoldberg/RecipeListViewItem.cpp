// Fill out your copyright notice in the Description page of Project Settings.

#include "RecipeListViewItem.h"

void URecipeListViewItem::OnSpawnButtonPressed(int RecipeIndex)
{
	if(RecipeIndex != Index)
	{
		return;
	}
	
	OnSpawnButtonPressedDelegate.Broadcast(RecipeIndex);
}

void URecipeListViewItem::OnCheckboxStateChanged(bool NewState, int RecipeIndex)
{
	if(RecipeIndex != Index)
	{
		return;
	}

	OnCheckboxChangedDelegate.Broadcast(NewState, RecipeIndex);
}
