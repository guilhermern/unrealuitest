// Fill out your copyright notice in the Description page of Project Settings.


#include "RecipeMachine.h"
#include "Recipe.h"
#include "Ingredient.h"
#include "RecipeListViewItem.h"

// Sets default values
ARecipeMachine::ARecipeMachine()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MainMesh"));
	RootComponent = Mesh;

	CollisionBox = CreateDefaultSubobject<UBoxComponent>("BoxCollision");
	CollisionBox->SetupAttachment(RootComponent);

	ResultSpawnPosition = CreateDefaultSubobject<USceneComponent>("ResultSpawnPosition");
	ResultSpawnPosition->SetupAttachment(RootComponent);
}

void ARecipeMachine::OnOverlapStarted(UPrimitiveComponent* OverlappedComponent, AActor* Other, UPrimitiveComponent* OtherComp, int32 OtherIndex, bool Sweep, const FHitResult &HitResult)
{
	AIngredient* OverlappedIngredient = Cast<AIngredient>(Other);

	if(OverlappedIngredient == nullptr)
	{
		return;
	}

	TSubclassOf<AIngredient> OtherClass = Other->GetClass();

	if(int32 Index; IsItemInCurrentIngredients(OtherClass, Index))
	{
		CurrentIngredients[Index].Quantity++;
	}
	else
	{
		FRecipeItem RecipeItem;
		RecipeItem.Item = OtherClass;
		RecipeItem.Quantity = 1;

		CurrentIngredients.Add(RecipeItem);
	}

	Other->Destroy();
}

void ARecipeMachine::CheckRecipes()
{
	int RecipeIndex;
	TArray<FRecipeItem>* ResultRecipeItems = GetRecipeFromInput(RecipeIndex);

	if(ResultRecipeItems == nullptr)
	{
		UE_LOG(LogTemp, Display, TEXT("NO MATCHING RECIPE"));
		return;
	}

	UE_LOG(LogTemp, Display, TEXT("YES! MATCHING RECIPE"));

	if(DisabledRecipes.Contains(RecipeIndex))
	{
		UE_LOG(LogTemp, Display, TEXT("RECIPE IS DISABLED FOR MACHINE! NOT SPAWNIG"));
		return;
	}

	for (int i = 0; i < ResultRecipeItems->Num(); i++)
	{
		TSubclassOf<AIngredient> IngredientToSpawn = (*ResultRecipeItems)[i].Item;
		int Quantity = (*ResultRecipeItems)[i].Quantity;

		for (int j = 0; j < Quantity; j++)
		{
			constexpr float ZOffset = 200;
			constexpr float XOffset = 150;

			FVector Location = ResultSpawnPosition->GetComponentLocation();
			Location.Z += j * ZOffset;
			Location.X += i * XOffset;

			FTransform SpawnTransform = FTransform(RootComponent->GetComponentRotation(), Location);

			FActorSpawnParameters SpawnParameters;
			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			GetWorld()->SpawnActor<AIngredient>(IngredientToSpawn, SpawnTransform, SpawnParameters);
		}
	}

	CurrentIngredients.Empty();
}

void ARecipeMachine::GetInputOutputStrings(const FRecipe* Recipe, FString &InputIngredients, FString &OutputIngredients)
{
	InputIngredients = "I: ";
	OutputIngredients = "O: ";

	for (int i=0; i < Recipe->Ingredients.Num(); i++)
	{
		FString Name = Recipe->Ingredients[i].Item->GetName();
		Name = Name.LeftChop(2);

		int Count = Recipe->Ingredients[i].Quantity;
		
		InputIngredients.Append(Name + " ");
		InputIngredients.Append(FString::FromInt(Count) + "x");

		if(i != (Recipe->Ingredients.Num() - 1))
		{
			InputIngredients.Append(", ");
		}
	}

	for (int i=0; i < Recipe->ResultIngredients.Num(); i++)
	{
		FString Name = Recipe->ResultIngredients[i].Item->GetName();
		Name = Name.LeftChop(2);

		int Count = Recipe->ResultIngredients[i].Quantity;
		
		OutputIngredients.Append(Name + " ");
		OutputIngredients.Append(FString::FromInt(Count) + "x");

		if(i != (Recipe->ResultIngredients.Num() - 1))
		{
			OutputIngredients.Append(", ");
		}
	}

}

TArray<FRecipeItem>* ARecipeMachine::GetRecipeFromInput(int& RecipeIndex)
{
	TArray<FRecipe*> Rows;
	
	Recipes->GetAllRows<FRecipe>(TEXT("Debug"), Rows);

	for (int i = 0; i < Rows.Num(); i++)
	{
		TArray<FRecipeItem>* CandidateRecipe = &Rows[i]->Ingredients;
		TArray<FRecipeItem>* ResultRecipe = &Rows[i]->ResultIngredients;

		if(IsRecipeMatching(CandidateRecipe))
		{
			RecipeIndex = i;
			return ResultRecipe;
		}
	}

	return nullptr;
}

bool ARecipeMachine::IsItemInCurrentIngredients(const TSubclassOf<AIngredient> Type, int32& OutIndex)
{
	for (int i = 0; i < CurrentIngredients.Num(); i++)
	{
		if(Type == CurrentIngredients[i].Item)
		{
			OutIndex = i;
			return true;
		}
	}

	return false;
}

void ARecipeMachine::PopulateListViewEntries()
{
	TArray<FRecipe*> Rows;

	Recipes->GetAllRows<FRecipe>(TEXT("Debug"), Rows);
	TArray<FName> RowsNames = Recipes->GetRowNames();

	for (int i = 0; i < Rows.Num(); i++)
	{
		FRecipe* Recipe = Rows[i];
		FName RecipeName = RowsNames[i];
		URecipeListViewItem* ViewItem = NewObject<URecipeListViewItem>();

		FString InputIngredients;
		FString OutputIngredients;
		GetInputOutputStrings(Recipe, InputIngredients, OutputIngredients);

		ViewItem->Index = i;
		ViewItem->TitleLabel = RecipeName.ToString();
		ViewItem->IngredientsLabel = InputIngredients;
		ViewItem->OutputLabel = OutputIngredients;
		
		ListViewEntriesItems.Add(ViewItem);
	}
}

void ARecipeMachine::DisableRecipe(int RecipeIndex)
{
	DisabledRecipes.AddUnique(RecipeIndex);
}

void ARecipeMachine::EnableRecipe(int RecipeIndex)
{
	DisabledRecipes.Remove(RecipeIndex);
}

bool ARecipeMachine::IsRecipeMatching(const TArray<FRecipeItem>* CandidateRecipe)
{
	if(CandidateRecipe->Num() != CurrentIngredients.Num())
	{
		return false;
	}

	bool IsRecipeMatching = true;
	for (int i = 0; i < CurrentIngredients.Num(); i++)
	{
		const FRecipeItem& CurrentItem = CurrentIngredients[i];

		if(!IsIngredientMatching(CurrentItem, CandidateRecipe))
		{
			IsRecipeMatching = false;
		}
	}

	return IsRecipeMatching;
}

bool ARecipeMachine::IsIngredientMatching(const FRecipeItem& CurrentItem, const TArray<FRecipeItem>* CandidateRecipe)
{
	for (int i = 0; i < CandidateRecipe->Num(); i++)
	{
		FRecipeItem CandidateItem = (*CandidateRecipe)[i];

		if(CurrentItem.Item == CandidateItem.Item && CurrentItem.Quantity == CandidateItem.Quantity)
		{
			return true;
		}
	}
	
	return false;
}

void ARecipeMachine::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &ARecipeMachine::OnOverlapStarted);
}

void ARecipeMachine::ForceSpawnRecipe(int RecipeIndex)
{
	TArray<FRecipe*> Rows;
	
	Recipes->GetAllRows(TEXT("Debug"), Rows);
	TArray<FRecipeItem>* ResultRecipeItems = &Rows[RecipeIndex]->ResultIngredients;
	
	for (int i = 0; i < ResultRecipeItems->Num(); i++)
	{
		TSubclassOf<AIngredient> IngredientToSpawn = (*ResultRecipeItems)[i].Item;
		int Quantity = (*ResultRecipeItems)[i].Quantity;

		for (int j = 0; j < Quantity; j++)
		{
			constexpr float ZOffset = 200;
			constexpr float XOffset = 150;

			FVector Location = ResultSpawnPosition->GetComponentLocation();
			Location.Z += j * ZOffset;
			Location.X += i * XOffset;

			FTransform SpawnTransform = FTransform(RootComponent->GetComponentRotation(), Location);

			FActorSpawnParameters SpawnParameters;
			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			GetWorld()->SpawnActor<AIngredient>(IngredientToSpawn, SpawnTransform, SpawnParameters);
		}
	}
}

// Called when the game starts or when spawned
void ARecipeMachine::BeginPlay()
{
	Super::BeginPlay();
	
	CollisionBox = FindComponentByClass<UBoxComponent>();
	PopulateListViewEntries();

	FTimerHandle TimerHandle;

	GetWorldTimerManager().SetTimer(TimerHandle, this, &ARecipeMachine::CheckRecipes, 3.0f, false);
}

// Called every frame
void ARecipeMachine::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
