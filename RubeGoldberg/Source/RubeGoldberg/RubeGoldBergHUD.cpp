// Fill out your copyright notice in the Description page of Project Settings.


#include "RubeGoldBergHUD.h"

#include "RecipeListViewItem.h"
#include "RecipeMachine.h"
#include "Components/ComboBoxString.h"
#include "Components/ListView.h"
#include "Kismet/GameplayStatics.h"
#include "Types/SlateEnums.h"

void URubeGoldBergHUD::PopulateComboBoxOptions()
{
	TArray<AActor*> FoundActors;
	
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), RecipeMachineClass, FoundActors);

	for (int i = 0; i < FoundActors.Num(); i++)
	{
		AActor* Actor = FoundActors[i];
		ComboBox->AddOption(Actor->GetActorNameOrLabel());
		int32 Index = ComboBox->GetOptionCount() - 1;

		ARecipeMachine* RecipeMachine = Cast<ARecipeMachine>(Actor);

		ComboBoxOptions.Add(Index, RecipeMachine);
	}

	SelectedIndex = 0;
	RecipesListView->SetVisibility(ESlateVisibility::Hidden);
}

void URubeGoldBergHUD::SetupListViewCallbacks(TArray<URecipeListViewItem*>& ListViewEntriesItems, TArray<int>& DisabledRecipes)
{
	for (int i = 0; i < ListViewEntriesItems.Num(); i++)
	{
		URecipeListViewItem* Item = ListViewEntriesItems[i];
		Item->OnSpawnButtonPressedDelegate.Clear();
	}
	
	for (int i = 0; i < ListViewEntriesItems.Num(); i++)
	{
		URecipeListViewItem* Item = ListViewEntriesItems[i];
		Item->OnSpawnButtonPressedDelegate.AddUniqueDynamic(this, &URubeGoldBergHUD::OnSpawnButtonPressed);
		Item->OnCheckboxChangedDelegate.AddUniqueDynamic(this, &URubeGoldBergHUD::OnEnabledCheckBoxChanged);

		Item->RecipeDisabled = DisabledRecipes.Contains(Item->Index);
	}
}

void URubeGoldBergHUD::OnSpawnButtonPressed(int RecipeIndex)
{
	ARecipeMachine* SelectedMachine = ComboBoxOptions[SelectedIndex];

	SelectedMachine->ForceSpawnRecipe(RecipeIndex);
}

void URubeGoldBergHUD::OnEnabledCheckBoxChanged(bool NewState, int RecipeIndex)
{
	ARecipeMachine* RecipeMachine = ComboBoxOptions[SelectedIndex];

	if(NewState)
	{
		RecipeMachine->EnableRecipe(RecipeIndex);
	}
	else
	{
		RecipeMachine->DisableRecipe(RecipeIndex);
	}
}

void URubeGoldBergHUD::OnComboBoxSelectionChanged(FString Item, ESelectInfo::Type SelectionType)
{
	SelectedIndex = ComboBox->GetSelectedIndex();

	UE_LOG(LogTemp, Warning, TEXT("ComboBoxSelection changed to %d"), SelectedIndex);

	if(SelectedIndex == 0)
	{
		RecipesListView->SetVisibility(ESlateVisibility::Hidden);
	}
	else
	{
		ARecipeMachine* RecipeMachine = ComboBoxOptions[SelectedIndex];
		TArray<URecipeListViewItem*>* ListViewEntriesItems = RecipeMachine->GetListViewEntries();
		TArray<int>* DisabledRecipes = RecipeMachine->GetDisabledRecipes();

		SetupListViewCallbacks(*ListViewEntriesItems, *DisabledRecipes);

		RecipesListView->ClearListItems();
		RecipesListView->SetListItems(*ListViewEntriesItems);
		RecipesListView->SetVisibility(ESlateVisibility::Visible);
	}
}

void URubeGoldBergHUD::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	ComboBox->OnSelectionChanged.AddDynamic(this, &URubeGoldBergHUD::OnComboBoxSelectionChanged);

	PopulateComboBoxOptions();
}

void URubeGoldBergHUD::OnFKeyPressed()
{
	CurrentVisibility = CurrentVisibility == ESlateVisibility::Hidden ? ESlateVisibility::Visible : ESlateVisibility::Hidden;
	
	SetVisibility(CurrentVisibility);
}
