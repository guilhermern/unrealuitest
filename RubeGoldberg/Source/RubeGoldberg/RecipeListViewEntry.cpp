// Fill out your copyright notice in the Description page of Project Settings.

#include "RecipeListViewEntry.h"
#include "RecipeListViewItem.h"
#include "Components/Button.h"
#include "Components/CheckBox.h"
#include "Components/TextBlock.h"

void URecipeListViewEntry::NativeOnListItemObjectSet(UObject* ListItemObject)
{
	URecipeListViewItem* Item = Cast<URecipeListViewItem>(ListItemObject);

	Index = Item->Index;
	TitleLabel->SetText(FText::FromString(Item->TitleLabel));
	IngredientsLabel->SetText(FText::FromString(Item->IngredientsLabel));
	OutputLabel->SetText(FText::FromString(Item->OutputLabel));

	OnSpawnButtonPressedDelegate.AddUniqueDynamic(Item, &URecipeListViewItem::OnSpawnButtonPressed);
	OnCheckboxChangedDelegate.AddUniqueDynamic(Item, &URecipeListViewItem::OnCheckboxStateChanged);
	
	SpawnResultButton->OnReleased.AddUniqueDynamic(this, &URecipeListViewEntry::OnSpawnButtonPressed);
	EnabledCheckBox->OnCheckStateChanged.AddUniqueDynamic(this, &URecipeListViewEntry::OnCheckboxChangedState);

	ECheckBoxState CheckBoxState = Item->RecipeDisabled ? ECheckBoxState::Unchecked : ECheckBoxState::Checked;

	EnabledCheckBox->SetCheckedState(CheckBoxState);
}

void URecipeListViewEntry::OnSpawnButtonPressed()
{
	OnSpawnButtonPressedDelegate.Broadcast(Index);
}

void URecipeListViewEntry::OnCheckboxChangedState(bool NewState)
{
	OnCheckboxChangedDelegate.Broadcast(NewState, Index);
	
}
