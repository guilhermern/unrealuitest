#pragma once

#include "Ingredient.h"
#include "FRecipeItem.generated.h"

USTRUCT(BlueprintType)
struct FRecipeItem
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AIngredient> Item;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Quantity;
};
