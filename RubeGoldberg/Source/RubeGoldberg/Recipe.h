// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FRecipeItem.h"
#include "Engine/DataTable.h"
#include "Recipe.generated.h"

USTRUCT(BlueprintType)
struct FRecipe : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FRecipeItem> Ingredients;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FRecipeItem> ResultIngredients;
};
