// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "Engine/DataTable.h"
#include "GameFramework/Actor.h"
#include "Ingredient.h"
#include "FRecipeItem.h"
#include "Recipe.h"
#include "RecipeMachine.generated.h"

UCLASS()
class RUBEGOLDBERG_API ARecipeMachine : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARecipeMachine();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent* CollisionBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USceneComponent* ResultSpawnPosition;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UDataTable* Recipes;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FName> RecipeNames;

	TArray<int> DisabledRecipes;

	UFUNCTION()
	void OnOverlapStarted(UPrimitiveComponent* OverlappedComponent, AActor* Other, UPrimitiveComponent* OtherComp, int32 OtherIndex, bool Sweep, const FHitResult &HitResult);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void PostInitializeComponents() override;

	FORCEINLINE TArray<class URecipeListViewItem*>* GetListViewEntries() { return &ListViewEntriesItems; }
	FORCEINLINE TArray<int>* GetDisabledRecipes() { return &DisabledRecipes; }

	void ForceSpawnRecipe(int RecipeIndex);

	void DisableRecipe(int RecipeIndex);
	void EnableRecipe(int RecipeIndex);

private:
	TArray<FRecipeItem> CurrentIngredients;
	TArray<URecipeListViewItem*> ListViewEntriesItems;

	UFUNCTION()
	void CheckRecipes();
	static void GetInputOutputStrings(const FRecipe* Recipe, FString &InputIngredients, FString &OutputIngredients);
	TArray<FRecipeItem>* GetRecipeFromInput(int& RecipeIndex);
	bool IsRecipeMatching(const TArray<FRecipeItem>* CandidateRecipe);
	static bool IsIngredientMatching(const FRecipeItem& CurrentItem, const TArray<FRecipeItem>* CandidateRecipe);
	bool IsItemInCurrentIngredients(TSubclassOf<AIngredient> Type, int32& OutIndex);
	void PopulateListViewEntries();
};
